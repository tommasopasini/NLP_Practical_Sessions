# Contacts
Valentina Pytakin: pyatkin@di.uniroma1.it  
Tommaso Pasini: pasini@di.uniroma1.it

# Get this repo locally
Install git on you computer (https://git-scm.com/) then just clone the project :  
- https: `git clone https://gitlab.com/tommasopasini/NLP_Practical_Sessions.git`

# NLP Practical Sessions
This git contains all resources, code and teaching material related to the practical sessions of the course as well as also the homeworks.
We will keep it up to date as lectures go on.

## 6 - Homework 2
In this second homework you have to implement a recurrent neural network POS-tagger.
You will use Universal dependencies English corpora for training developing and testing.
To obtain extra point you can use Italin corpora in order to create a neural network that is
able to POS tag sentences in English and Italian.

## 5 - Character based language modelling with keras
Here we show how to implement a char based language model with Keras.
It can be usefull if you need to predixt the next word given an history of character (not words).

## 4 - Language modelling with keras
How to implement a neural language model.
Introduction to keras and the most important layers for language modelling (LSTM).

## 3 - Language modelling practical session
How to implement a simple statistical language model in python.
You can improve the implementation adding smoothing factor to probabilities ecc.


## 2 - Homework 1
We just release the homework 1 which you can download simplying running   
`git clone https://gitlab.com/tommasopasini/NLP_Practical_Sessions.git # in case you didn't download the repository yet`  
`git pull #inside your local repository folder NLP_Practical_Sessions`  
Inside the homework_1 folder you will find the Slides and the files presentend during the lecture.

## 1 - Morphology practical session

### What to install
 - Jupyter http://jupyter.org/install.html .  
   download anaconda python 2.7 https://www.continuum.io/downloads -> `chmod +x ./Anaconda2-4.3.0-Linux-x86_64.sh; ./Anaconda2-4.3.0-Linux-x86_64.sh`
 - BeakerNotebook http://beakernotebook.com/getting-started     
   download the package for you SO, then `cd dir; chmod +x *.sh; ./install-dependencies.sh ; ./install-runtime-core-dependencies.sh`
 - nltk http://www.nltk.org/     
   `sudo pip install nltk`
 - morfessor https://github.com/aalto-speech/morfessor     
   `git clone https://github.com/aalto-speech/morfessor` -> `cd morfessor-master; sudo python setup.py install` 
 
### Overview

In this session we will introduce you to the NLP pipeline and to some tools (for both Java and Python) that you will find usefull during the course (e.g. NLTK, Stanford CoreNLP).  
Then we will practice a bit with Morfessor, a program that performs the morphological splitting of words in any language.

### Directory tree
1-morphology-practical-session  
&nbsp;&nbsp;&nbsp;&nbsp; - corenlp demo of Stanford CoreNLP toolkit  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - libs libraries for Stanford CoreNLP  
&nbsp;&nbsp;&nbsp;&nbsp; - morfessor demo of Morfessor   
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - data pretrained models  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - output_models output directory of the demostration  
&nbsp;&nbsp;&nbsp;&nbsp; - nltk demo of nltk framework